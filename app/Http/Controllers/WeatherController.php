<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Inertia\Inertia;

class WeatherController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $url = 'https://api.openweathermap.org/data/2.5/weather?q=kuressaare&units=metric&appid=9d0e2684a0ab4bcab819e85bdf5944c8';
        $data = $this->cacheData($url);
        return Inertia::render('Weather', [
            'data' => $data
        ]);
    }
    private function cacheData($url)
    {
        if(!Cache::has('weather')){
            $query = Http::get($url)->json();
            Cache::put('weather', $query, now()->addHour());
        }
        return Cache::get('weather');
    }
}